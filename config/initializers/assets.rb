Rails.application.config.assets.precompile += %w( application_header.js )
Rails.application.config.assets.precompile += %w( application_footer.js )
Rails.application.config.assets.precompile += %w( datatables/dataTables.bootstrap.css )
Rails.application.config.assets.precompile += %w( datatables/js/jquery.dataTables.min.js )
Rails.application.config.assets.precompile += %w( datatables/dataTables.bootstrap.js )
Rails.application.config.assets.precompile += %w( datatables/yadcf/jquery.dataTables.yadcf.js )
Rails.application.config.assets.precompile += %w( datatables/tabletools/dataTables.tableTools.min.js )
Rails.application.config.assets.precompile += %w( fonts/meteocons/css/meteocons.css )
Rails.application.config.assets.precompile += %w( jvectormap/jquery-jvectormap-1.2.2.min.js )
Rails.application.config.assets.precompile += %w( jquery.timeago.js )
Rails.application.config.assets.precompile += %w( codemirror/doc/docs.css )
Rails.application.config.assets.precompile += %w( codemirror/lib/codemirror.css )
Rails.application.config.assets.precompile += %w( codemirror/addon/hint/show-hint.css )
Rails.application.config.assets.precompile += %w( codemirror/theme/base16-light.css )
Rails.application.config.assets.precompile += %w( codemirror/theme/mdn-like.css )
Rails.application.config.assets.precompile += %w( codemirror/theme/monokai.css )
Rails.application.config.assets.precompile += %w( codemirror/theme/solarized.css )
Rails.application.config.assets.precompile += %w( codemirror/lib/codemirror.js )
Rails.application.config.assets.precompile += %w( codemirror/mode/clike/clike.js )
Rails.application.config.assets.precompile += %w( codemirror/mode/python/python.js )
Rails.application.config.assets.precompile += %w( codemirror/addon/edit/matchbrackets.js )
Rails.application.config.assets.precompile += %w( codemirror/addon/hint/show-hint.js )
Rails.application.config.assets.precompile += %w( codemirror/addon/hint/javascript-hint.js )
Rails.application.config.assets.precompile += %w( codemirror/mode/javascript/javascript.js )
Rails.application.config.assets.precompile += %w( jquery-ui/jquery-ui.min.js )
Rails.application.config.assets.precompile += %w( selectboxit/jquery.selectBoxIt.min.js )
Rails.application.config.assets.precompile += %w( code-editor.js )
Rails.application.config.assets.precompile += %w( submissions.js )